package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/*
* Controller de l'interface
*/
@Controller
@RequestMapping(path="/demo") // This means URL's start with /demo (after Application path)
public class GreetingController {

    @Autowired // This means to get the bean called userRepository
	           // Which is auto-generated by Spring, we will use it to handle the data
    private ParticipantRepository participant_repository;
    
    /*
    * Methode d'ajout d'un participant par remplissage du formulaire
    */
    @GetMapping(path="/addpart") // Map ONLY GET Requests
    public @ResponseBody String addNewParticipant (@RequestParam String nom, 
                    @RequestParam String prenom,
                    @RequestParam String email,
                    @RequestParam String datenaiss,
                    @RequestParam String organisation,
                    @RequestParam String obs,
                    @RequestParam Evenement participe) {
            // @ResponseBody means the returned String is the response, not a view name
            // @RequestParam means it is a parameter from the GET or POST request

            Participant n = new Participant(nom, prenom, email, datenaiss, organisation, obs,participe);
            /*n.setName(name);
            n.setEmail(email);*/
            participant_repository.save(n);
            return "Saved";
    }
    
    /*
    * Affiche la liste des participants enregistrés
    */
    @GetMapping(path="/allpart")
    public @ResponseBody Iterable<Participant> getAllPart() {
            // This returns a JSON or XML with the users
            return participant_repository.findAll();
    }
    
    @Autowired
    private EvenementRepository evenement_repository;
    
    /*
    * Ajout d'un évènement par remplissage du formulaire
    */
    @GetMapping(path="/addev") // Map ONLY GET Requests
    public @ResponseBody String addNewEvenement (@RequestParam String intitule
                    , @RequestParam String theme,
                    @RequestParam String dateDeb,
                    @RequestParam String duree,
                    @RequestParam int nbPartMax,
                    @RequestParam String desc,
                    @RequestParam String organisateur,
                    @RequestParam String typeEven) {
            // @ResponseBody means the returned String is the response, not a view name
            // @RequestParam means it is a parameter from the GET or POST request

            Evenement n = new Evenement(intitule, theme, dateDeb, nbPartMax, nbPartMax, desc, organisateur, typeEven);
            /*n.setName(name);
            n.setEmail(email);*/
            evenement_repository.save(n);
            return "Saved";
    }
    
    /*
    * Liste des évènements enregistrés
    */
    @GetMapping(path="/allev")
    public @ResponseBody Iterable<Evenement> getAllEv() {
            // This returns a JSON or XML with the users
            return evenement_repository.findAll();
    }
}

