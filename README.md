# TP J2E Hibernate

 

## Connexion à la Base de Données

Hibernate s'occupe de la connexion à la base de données. Son rôle est également de gérer la persistance d'objets en bases de données relationnelles et les objets Java. La base de données doit être déjà existante. Le paramétrage de la connexion à la base de données par Hibernate se fait avec le fichier application.properties :

 

 

`*spring.jpa.hibernate.ddl-auto=update*`

 

`*spring.datasource.url=jdbc:postgresql://localhost:5432/hibernate*`

 

`*spring.datasource.username=formation*`

 

`*spring.datasource.password=formation*`

